//
//  DBManager.h
//  SQLite3DBSample
//
//  Created by pcphasemm006 on 6/10/15.
//  Copyright (c) 2015 pcphasemm006. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject

// Properties list
@property (nonatomic, strong)NSString *documentsDirectory;
@property (nonatomic, strong)NSString *databaseFilename;

// Method lists
- (instancetype)initWithDatabaseFilename:(NSString *)dbFilename;
- (void)copyDatabaseIntoDocumentsDirectory;

@end
