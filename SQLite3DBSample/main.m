//
//  main.m
//  SQLite3DBSample
//
//  Created by pcphasemm006 on 6/10/15.
//  Copyright (c) 2015 pcphasemm006. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
