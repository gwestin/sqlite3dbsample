//
//  AppDelegate.h
//  SQLite3DBSample
//
//  Created by pcphasemm006 on 6/10/15.
//  Copyright (c) 2015 pcphasemm006. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

