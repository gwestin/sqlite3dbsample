//
//  DBManager.m
//  SQLite3DBSample
//
//  Created by pcphasemm006 on 6/10/15.
//  Copyright (c) 2015 pcphasemm006. All rights reserved.
//

#import "DBManager.h"

@implementation DBManager

- (instancetype)initWithDatabaseFilename:(NSString *)dbFilename {
    self = [super init];
    
    if (self) {
        // set the document directory path to the documentsDirectory property
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
        
        // keep the database filename
        self.databaseFilename = dbFilename;
        
        // copy the database file into the documents directory if necessary
        [self copyDatabaseIntoDocumentsDirectory];
    }
    
    return self;
}

# pragma mark - 

@end
